TUGAS 10 SQL

Soal 1 Membuat Database
Jawaban : 
CREATE DATABASE myshop;

=======================================================================================

Soal 2 Membuat Table di Dalam Database
Jawaban :
CREATE TABLE users (
	  id INT(8) PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255),
    email VARCHAR(255),
    password VARCHAR(255)
);

CREATE TABLE categories (
	  id INT(8) PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255)
);

CREATE TABLE items (
	  id INT(8) PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255),
    description	VARCHAR(255),
    price INT(8),
    stock INT(8),
    category_id INT(8),
    FOREIGN KEY (category_id) REFERENCES categories(id)
);

=======================================================================================

Soal 3 Memasukkan Data pada Table
Jawaban :
1. INSERT INTO users (name, email, password) VALUES ("John Doe", "john@doe.com", "john123"), ("Jane Doe", "jane@doe.com", "jenita123");


2. INSERT INTO categories (name) VALUES ("gadget"),("cloth"),("men"),("women"),("branded");

3. INSERT INTO items (name, description, price, stock, category_id) VALUES ("sumsang", "b50	hape keren dari merek sumsang", 4000000, 100, 1), ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2), ("IMHO","Watch	jam tangan anak yang jujur banget", 2000000, 10, 1);

=======================================================================================

Soal 4 Mengambil Data dari Database
Jawaban:
a. Mengambil data users
SELECT id, name, email FROM users;

b. Mengambil data items
- SELECT * FROM items WHERE price > 1000000;
- SELECT * FROM items WHERE name LIKE '%sang%';

c. Menampilkan data items join dengan kategori
SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name FROM items INNER JOIN categories on items.category_id = categories.id;

=======================================================================================

Soal 5 Mengubah Data dari Database
Jawaban :

UPDATE items SET price = 2500000 WHERE id = 1;
@extends('layouts.master')
@section('content')

<div class="content-wrapper">
  <div class="content-header">
    <div class="container-fluid">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Detail Pemain Film</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <div class="card card-primary card-outline">
            <div class="card-body box-profile">
              <div class="text-center">
                <img class="profile-user-img img-fluid img-circle"
                     src="{{asset('assets/dist/img/user4-128x128.jpg')}}"
                     alt="User profile picture">
              </div>

              <h3 class="profile-username text-center">{{$cast->nama}}</h3>

              <p class="text-muted text-center">{{$cast->bio}}</p>
              <p class="text-muted text-center">{{$cast->umur}} Tahun</p>

              <ul class="list-group list-group-unbordered mb-3">
                <li class="list-group-item">
                  <b>Followers</b> <a class="float-right">1,322</a>
                </li>
                <li class="list-group-item">
                  <b>Following</b> <a class="float-right">543</a>
                </li>
                <li class="list-group-item">
                  <b>Friends</b> <a class="float-right">13,287</a>
                </li>
              </ul>

              <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
</div>

@endsection
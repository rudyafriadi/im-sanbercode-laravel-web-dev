@extends('layouts.master')
@section('content')
<div class="content-wrapper">
  <div class="content-header">
    <div class="container-fluid">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Halaman Cast</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <a href="/cast/create" class="btn btn-success">
             Tambah
          </a>
          {{-- <a href="/cast/create" type="button"> Tambah </a> --}}
          {{-- <button type="button" class="btn btn-block btn-primary btn-lg">Tambah</button> --}}
          <br>
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Nama</th>
              <th>Umur</th>
              <th>Bio</th>
              <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
              @foreach ($cast as $data)
              <tr>
                <td>{{$data->nama}}</td>
                <td>{{$data->umur}}</td>
                <td>{{$data->bio}}</td>
                <td>
                  <a href="/cast/detail/{{$data->id}}" class="btn btn-success">
                    Detail
                  </a>
                  <a href="/cast/edit/{{$data->id}}" class="btn btn-warning">
                    Edit
                  </a>
                  <a href="/cast/delete/{{$data->id}}" class="btn btn-danger">
                    Delete
                  </a>
                </td>
              </tr>
              @endforeach
            
            </tbody>
            {{-- <tfoot>
            <tr>
              <th>Rendering engine</th>
              <th>Browser</th>
              <th>Platform(s)</th>
              <th>Engine version</th>
              <th>CSS grade</th>
            </tr>
            </tfoot> --}}
          </table>
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
</div>
@endsection

@push('scripts')
    
<script src="{{asset('assets/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush
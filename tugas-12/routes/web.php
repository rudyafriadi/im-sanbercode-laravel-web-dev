<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/', [HomeController::class, 'index']);
// Route::get('/register', [AuthController::class, 'register']);
// Route::get('/welcome', [AuthController::class, 'welcome']);

Route::get('/', [HomeController::class, 'index']);
Route::get('/table', [HomeController::class, 'table']);
Route::get('/data-tables', [HomeController::class, 'datatables']);


Route::prefix('cast')->group(function(){
    Route::get('/', [CastController::class, 'index']);
    Route::get('/create', [CastController::class, 'create']);
    Route::post('/insert', [CastController::class, 'insert']);
    Route::get('/detail/{id}', [CastController::class, 'detail']);
    Route::get('/edit/{id}', [CastController::class, 'edit']);
    Route::post('/update/{id}', [CastController::class, 'update']);
    Route::get('/delete/{id}', [CastController::class, 'destroy']);
});

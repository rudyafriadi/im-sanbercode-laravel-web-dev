<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('beranda');
    }

    public function table()
    {
        return view('pages.table');
    }

    public function datatables()
    {
        return view('pages.datatables');
    }
}

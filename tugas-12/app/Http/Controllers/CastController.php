<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \DB;

class CastController extends Controller
{
    public function index()
    {
        $cast = DB::table('cast')->get();
        return view('pages.cast', compact('cast'));
    }

    public function create()
    {
        return view('pages.post');
    }

    public function insert(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast');
    }

    public function detail($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('pages.detail', compact('cast'));
    }


    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('pages.edit', compact('cast'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $query = DB::table('cast')
        ->where('id', $id)
        ->update([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast');
    }


    public function destroy($id)
    {
        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
